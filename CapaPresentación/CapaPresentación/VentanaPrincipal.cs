﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaLogica;


namespace CapaPresentación
{
    public partial class VentanaPrincipal : Form
    {
        public VentanaPrincipal()
        {
            InitializeComponent();
        }

        LEstudiante estudiante = new LEstudiante();

        public void soloNumeros(KeyPressEventArgs e)
        {
            try
            {
                if (char.IsNumber(e.KeyChar))
                {
                    e.Handled = false;
                }
                else if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }

            }
            catch (Exception)
            {
            }


        }
        public void LimpiarDatosEstudiante()
        {
            txtnombre.Text = "";
            txtnota.Text = "";
            txtcedula.Text = "";
            cbxcarrera.Text = "Carrera";
            cbxestado.Text = "Estado";
            cbxmateria.Text = "Materia";
        }

        public void cargarEstudiantes(string estado)
        {
            dgvEstudiantes.DataSource = null;
            dgvEstudiantes.DataSource = estudiante.CargarEstudiante(estado);
        }

        private void VentanaPrincipal_Load(object sender, EventArgs e)
        {
            cargarEstudiantes("");
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (txtnombre.Text != "" && txtcedula.Text != "" && txtnota.Text != "" && cbxcarrera.Text != "Carrera" && cbxmateria.Text != "Materia" && cbxestado.Text != "Estado")
            {
                estudiante.RegistrarEstudiante(txtnombre.Text, txtcedula.Text, cbxcarrera.Text, cbxmateria.Text, Convert.ToDecimal(txtnota.Text), cbxestado.Text);
                MensajeEstudiante.Text = "Registro exitoso";
                LimpiarDatosEstudiante();
                cargarEstudiantes("");

            }
            else
            {
                MensajeEstudiante.Text = "Debe completar todos los espacios";
            }
        }

        private void txtnota_KeyPress(object sender, KeyPressEventArgs e)
        {
            soloNumeros(e);
        }

        private void txtcedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            soloNumeros(e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cbxFiltroEstado.Text != "Estado")
            {
                string estado;
                if (cbxFiltroEstado.Text.Equals("Todos"))
                {
                    estado = "";
                }
                else
                {
                    estado = cbxFiltroEstado.Text;
                }
                cargarEstudiantes(estado);
            }
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            try
            {
                mensajeSeleccion.Text = "Estudiante seleccionado:";
                codigoSeleccionado.Text = dgvEstudiantes.CurrentRow.Cells[0].Value.ToString();
                cbxcarrera.Text = dgvEstudiantes.CurrentRow.Cells[3].Value.ToString();
                cbxmateria.Text = dgvEstudiantes.CurrentRow.Cells[4].Value.ToString();
                txtnota.Text = dgvEstudiantes.CurrentRow.Cells[5].Value.ToString();
                cbxestado.Text = dgvEstudiantes.CurrentRow.Cells[6].Value.ToString();
                txtcedula.Text = dgvEstudiantes.CurrentRow.Cells[2].Value.ToString();
                txtnombre.Text = dgvEstudiantes.CurrentRow.Cells[1].Value.ToString();
            }
            catch
            {
                mensajeSeleccion.Text = "Error al seleccionar";
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                if (codigoSeleccionado.Text != "")
                {
                    estudiante.EditarEstudiante(txtnombre.Text, txtcedula.Text, cbxcarrera.Text, cbxmateria.Text, Convert.ToDecimal(txtnota.Text), cbxestado.Text, Convert.ToInt32(codigoSeleccionado.Text));
                    cargarEstudiantes("");
                    MensajeEstudiante.Text = "Edición exitosa";
                    LimpiarDatosEstudiante();
                }
            }
            catch
            {
                MensajeEstudiante.Text = "Algo salió mal, intente nuevamente";
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvEstudiantes.CurrentRow.Cells[0].Value.ToString() != "")
                {
                    estudiante.EliminarEstudiante(Convert.ToInt32(dgvEstudiantes.CurrentRow.Cells[0].Value.ToString()));
                    cargarEstudiantes("");
                    MensajeEstudiante.Text = "Eliminación exitosa";
                }
            }catch
            {
                MensajeEstudiante.Text = "Algo salió mal, intente nuevamente";
            }
        }
    }
}