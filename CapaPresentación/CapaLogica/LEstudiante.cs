﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;

namespace CapaLogica
{
    public class LEstudiante
    {
        public void RegistrarEstudiante(string nombre, string cedula, string carrera, string materia, decimal nota, string estado)
        {
            using (ControlDeEstudianteEntities db = new ControlDeEstudianteEntities())
            {
                estudiante estudiante = new estudiante();
                estudiante.nombre = nombre;
                estudiante.cedula = cedula;
                estudiante.carrera = carrera;
                estudiante.materia = materia;
                estudiante.nota = nota;
                estudiante.estado = estado;
                db.estudiante.Add(estudiante);
                db.SaveChanges();
            }

        }

        public List<EEstudiante> CargarEstudiante(string estado)
        {
            using (ControlDeEstudianteEntities db = new ControlDeEstudianteEntities())
            {
                List<EEstudiante> lista = new List<EEstudiante>();
                var lst = from estudiante in db.estudiante
                          where estudiante.estado.Contains(estado)
                          select estudiante;
                foreach (var i in lst)
                {
                    EEstudiante estu = new EEstudiante();
                    estu.codigo = Convert.ToString(i.codigo);
                    estu.nombre = i.nombre;
                    estu.cedula = i.cedula;
                    estu.carrera = i.carrera;
                    estu.materia = i.materia;
                    estu.nota = i.nota;
                    estu.estado = i.estado;
                    lista.Add(estu);
                }
                return lista;
            }

        }

        public void EditarEstudiante (string nombre, string cedula, string carrera, string materia, decimal nota, string estado, int id)
        {

            using (ControlDeEstudianteEntities db = new ControlDeEstudianteEntities())
            {
                estudiante estudiante = null;

                estudiante = db.estudiante.Find(id);
                estudiante.nombre = nombre;
                estudiante.cedula = cedula;
                estudiante.carrera = carrera;
                estudiante.materia = materia;
                estudiante.nota = nota;
                estudiante.estado = estado;
                db.Entry(estudiante).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }


        }

        public void EliminarEstudiante (int id)
        {
            using (ControlDeEstudianteEntities db = new ControlDeEstudianteEntities())
            {
                estudiante estudiante;
                estudiante = db.estudiante.Find(id);
                db.estudiante.Remove(estudiante);
                db.SaveChanges();
            }

        }






    }
}
