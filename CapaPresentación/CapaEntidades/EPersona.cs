﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public abstract class EPersona
    {
        public string cedula { get; set; }
        public string nombre { get; set; }

        public EPersona()
        {
        }

        public EPersona(string cedula, string nombre)
        {
            cedula = cedula;
            nombre = nombre;
        }
    }




}
