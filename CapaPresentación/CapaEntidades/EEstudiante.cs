﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CapaEntidades
{
    public class EEstudiante : EPersona
    {
        public string codigo { get; set; }
        public string carrera { get; set; }
        public string materia { get; set; }
        public decimal nota { get; set; }
        public string estado { get; set; }

        public EEstudiante() : base()
        {
        }

        public EEstudiante(string codigo, string carrera, string materia, decimal nota, string estado, string cedula, string nombre) : base (cedula, nombre)
        {
            this.codigo = codigo;
            this.carrera = carrera;
            this.materia = materia;
            this.nota = nota;
            this.estado = estado;
        }
    }
}
